#include "matrix.hpp"
const int INFINITY = 2147483000; // max integer

matrix::matrix(int vert, int edg, int start) : graph(vert, edg, start), neigh_mat(std::make_unique<std::unique_ptr<int[]>[]>(vert))
{
    for (int i = 0; i < num_vertices; i++)
    {
        neigh_mat[i] = std::make_unique<int[]>(num_vertices);
        for (int j = 0; j < num_vertices; j++)
            neigh_mat[i][j] = 0;
    }
}

const int matrix::print()
{
    for (int i = 0; i < ret_vert(); i++)
    {
        for (int j = 0; j < ret_vert(); j++)
        {
            std::cout << neigh_mat[i][j] << "   ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    return 0;
}

const int matrix::fill(int verts, int edges)
{

    num_edges = edges;
    num_vertices = verts;
    num_start = 0;
    neigh_mat = std::make_unique<std::unique_ptr<int[]>[]>(num_vertices);

    for (int j = 0; j < num_vertices; j++)
    {
        neigh_mat[j] = std::move(std::make_unique<int[]>(num_vertices));
        for (int k = 0; k < num_vertices; k++)
        {
            neigh_mat[j][k] = 0;
        }
    }

    if (edges == (verts * (verts - 1))) // graf pelny->gest=1
    {
        for (int row = 0; row < verts; ++row)
        {
            for (int col = 0; col < verts; ++col)
            {

                if (row != col)
                {
                    int wigh = rand() % 15;     //losuje wage polaczenia
                    while (wigh == 0)
                    {
                        wigh = rand() % 15;
                    }
                    neigh_mat[row][col] = wigh;
                }
            }
        }
    }

    else // gest!=1
    {
        int edges = num_edges;
        while (edges)
        {
            int rown = rand() % num_vertices;       //losuje polaczenie i wage
            int coln = rand() % num_vertices;

            if (neigh_mat[rown][coln] == 0)
            {
                int wigh = rand() % 15;
                while (wigh == 0)
                {
                    wigh = rand() % 15;
                }

                if (rown != coln)
                {
                    neigh_mat[rown][coln] = wigh;
                    --edges;
                }
            }
        }
    }

    return 0;
}

const int matrix::read_file(const char *nazwa)
{
    std::ifstream input(nazwa);
    if (!input.is_open())
    {
        std::cout << "file did not open" << std::endl;
    }
    int from, to, weight;
    input >> num_edges >> num_vertices >> num_start; // pierwsza linijka pliku
    neigh_mat = std::make_unique<std::unique_ptr<int[]>[]>(num_vertices);

    for (int j = 0; j < num_vertices; j++)
    {
        neigh_mat[j] = std::move(std::make_unique<int[]>(num_vertices));
        for (int k = 0; k < num_vertices; k++)
        {
            neigh_mat[j][k] = 0;        
        }
    }
    for (int i = 0; i < num_edges; i++)
    {                                       // zczytuje odpowiednie wartosci i przypisuje do komorek w macierzy
        input >> from >> to >> weight;
        neigh_mat[from][to] = weight;
    }
    input.close();
    return 0;
}

const int matrix::dijkstra()
{
    int verts = num_vertices;
    int start = num_start;

    int *dist = new int[verts]; // tablica z kosztami dojscia
    int *prev = new int[verts]; // tablica z numerem poprzedniego wierzcholkiem
    int *path = new int[verts]; // tablica w ktorej bedziemy zapisywac kolejnosc wierzcholkow
    int pathptr = 0;

    bool *checked = new bool[verts]; // czy wierzcholek sprawdzony

    for (int i = 0; i < verts; i++) // wypelniam tablice z kosztami i numerami wierzcholkow
    {
        if (i != start)
        {
            dist[i] = INFINITY;
            prev[i] = -1;
            checked[i] = false;
        }
        else
        {
            dist[i] = 0;
            prev[i] = -1;
            checked[i] = false;
        }
    }

    for (int i = 0; i < verts; i++)
    {
        for (int j = 0; j < verts; j++)
        {

            if (neigh_mat[i][j] != 0)
            {
                if (checked[i] == false && dist[j] > dist[i] + neigh_mat[i][j]) //sprawdzam czy odleglosc przypisana wierzcholkowi wieksza od aktualnie zapisywanej
                {
                    dist[j] = dist[i] + neigh_mat[i][j];
                    prev[j] = i;
                }
            }
        }
        checked[i] = true;
    }

    std::ofstream output("wyjm.txt");

    for (int w = 0; w < verts; w++) //wypisuje odpowiednio sciezke do wierzcholkow do pliku wyjsciowego
    {
        if (start != w)
        {
            output << w << ":   ";

            for (int j = w; j > -1; j = prev[j])
                path[pathptr++] = j;

            while (pathptr)
                output << "-->" << path[--pathptr];

            output << "   $$ wartosc: " << dist[w] << std::endl;
        }
    }

    delete[] dist;  //usuwam dynamiczne tablice

    delete[] prev;

    delete[] checked;

    delete[] path;

    return 0;
}