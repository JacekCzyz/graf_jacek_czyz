#include "list.hpp"
const int INFINITY = 2147483000; // max integer

const int list::print()
{
    for (int i = 0; i < num_vertices; i++)  //wypisuje po kolei wierzcholki z ktorymi lacza sie punkty
    {
        std::cout << i << ": ";
        node *ptr = head[i];
        if (ptr == nullptr)
        {
            std::cout << "---";
        }
        while (ptr != nullptr)
        {
            std::cout << "->"
                      << "(dest:" << ptr->num << ", weight:" << ptr->weight << ")";
            ptr = ptr->next;
        }
        std::cout << std::endl;
    }
    return 0;
}

const int list::read_file(const char *nazwa)
{
    std::ifstream input(nazwa);
    if (!input.is_open())
    {
        std::cout << "file did not open" << std::endl;
    }

    int from, to, weight;
    input >> num_edges >> num_vertices >> num_start; // pierwsza linijka pliku

    head = new node *[num_vertices];

    for (int i = 0; i < num_vertices; i++)
    {
        head[i] = nullptr; // inicjuje 'heady' dla wsyzstkich wierzchołków
    }
    for (int i = 0; i < num_edges; i++)     //wczytuje odpowiednie wartosci i inicjuje kolejnego heada
    {
        input >> from >> to >> weight;
        int src = from;
        int dest = to;
        int wigh = weight;

        node *newnode = lists(dest, wigh, head[src]);
        head[src] = newnode;
    }
    input.close();
    return 0;
}

int check(node *head, int *dist, int *prev, int i, bool *checked)
{
    int cur_num = head->num;
    int cur_weig = head->weight;

    if (checked[i] == false && (dist[cur_num]) > (dist[i] + cur_weig))      //sprawdzam czy odleglosc przypisana wierzcholkowi wieksza od aktualnie zapisywanej
    {
        dist[cur_num] = dist[i] + cur_weig;
        prev[cur_num] = i;
    }
    if (head->next != nullptr)
        check(head->next, dist, prev, i, checked);
    return 0;
}

const int list::fill(int verts, int edges)
{

    num_edges = edges;
    num_vertices = verts;
    num_start = 0;

    head = new node *[num_vertices];
    for (int i = 0; i < num_vertices; i++)
    {
        head[i] = nullptr; // inicjuje 'heady' dla wszystkich wierzchołków
    }

    if (edges == (verts * (verts - 1))) // graf pelny->gest=1
    {
        for (int from = 0; from < verts; ++from)
        {
            for (int to = 0; to < verts; ++to)
            {
                int src = from;     
                int dest = to;          
                int wigh = rand() % 15;     //losuje wage polaczenia
                while (wigh == 0)
                {
                    wigh = rand() % 15;
                }
                node *newnode = lists(dest, wigh, head[src]);
                head[src] = newnode;    //przypisuje headowi nowy element
            }
        }
    }

    else // gest!=1
    {
        int fooedges = 0;
        while (fooedges < num_edges)
        {
            int src = rand() % num_vertices;
            int dest = rand() % num_vertices;       //losuje polaczenie i wage
            int wigh = rand() % 15;
            while (wigh == 0)
            {
                wigh = rand() % 15;
            }
            if (src != dest)
            {
                node *newnode = lists(dest, wigh, head[src]);   
                head[src] = newnode;    
                fooedges++;
            }
        }
    }

    return 0;
}

const int list::dijkstra()
{
    int verts = num_vertices;
    int start = num_start;

    int *dist = new int[verts]; // tablica z kosztami dojscia
    int *prev = new int[verts]; // tablica z numerem poprzedniego wierzcholkiem
    int *path = new int[verts]; // tablica w ktorej bedziemy zapisywac kolejnosc wierzcholkow
    int pathptr = 0;

    bool *checked = new bool[verts]; // czy wierzcholek sprawdzony

    for (int i = 0; i < verts; i++)
    {
        if (i != start)
        {
            dist[i] = INFINITY;
            prev[i] = -1;
            checked[i] = false;
        }
        else
        {
            dist[i] = 0;
            prev[i] = -1;
            checked[i] = false;
        }
    }

    for (int i = 0; i < verts; i++)
    {
        if (head[i] != nullptr)
        {
            check(head[i], dist, prev, i, checked);     //uruchamiam funkcje sprawdzajaca
        }
    }


    std::ofstream output("wyjl.txt");

    for (int i = 0; i < verts; i++)     //wypisuje odpowiednio sciezke do wierzcholkow do pliku wyjsciowego
    {
        if (start != i)
        {
            output << i << ":    ";

            for (int j = i; j > -1; j = prev[j])
                path[pathptr++] = j;

            while (pathptr)
                output << "-->" << path[--pathptr];

            output << "   $$ wartosc: " << dist[i] << std::endl;
        }
    }


    delete[] dist;  //usuwam dynamiczne tablice

    delete[] prev;

    delete[] checked;

    delete[] path;

    return 0;
}