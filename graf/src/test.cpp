#include "matrix.hpp"
#include "list.hpp"
#include <chrono>

int main()
{
    srand(static_cast<unsigned int>(time(NULL)));

    double dens[4] = {0.25, 0.50, 0.75, 1};
    int vertices[5] = {10, 25, 50, 75, 100};
    double summ[4][5], suml[4][5];
    double timem, timel;
    std::ofstream outputl("wyjtl.txt");
    std::ofstream outputm("wyjtm.txt");

    int choice;
    std::cout << "tryb uruchomienia: 1-test wczytania pliku, 2-pelne testy" << std::endl;
    std::cin >> choice;

    switch (choice)
    {
    case 1:
    {
        matrix grafm;
        std::shared_ptr<list> grafl = std::make_shared<list>(6, 9, 0);

        grafm.read_file("wej.txt");
        grafl->read_file("wej.txt");

        grafm.print();
        grafl->print();

        grafm.dijkstra();
        grafl->dijkstra();
        break;
    }

    case 2:
    {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                timel = 0;
                timem = 0;
                for (int k = 0; k < 100; k++)
                {
                    int edges = (int)(dens[i] * vertices[j] * (vertices[j] - 1));

                    std::shared_ptr<list> grafl = std::make_shared<list>(vertices[j], edges, 0);
                    grafl->fill(vertices[j], edges);

                    auto t_startl = std::chrono::steady_clock::now();
                    grafl->dijkstra();
                    auto t_endl = std::chrono::steady_clock::now();
                    timel += std::chrono::duration_cast<std::chrono::milliseconds>(t_endl - t_startl).count();

                    std::shared_ptr<matrix> grafm = std::make_shared<matrix>(vertices[j], edges, 0);
                    grafm->fill(vertices[j], edges);

                    auto t_startm = std::chrono::steady_clock::now();
                    grafm->dijkstra();
                    auto t_endm = std::chrono::steady_clock::now();
                    timem += std::chrono::duration_cast<std::chrono::milliseconds>(t_endm - t_startm).count();
                }
                suml[i][j] = timel / 100;
                summ[i][j] = timem / 100;
            }
        }

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                outputl << "dens: " << dens[i] << "  num_of_verts: " << vertices[j] << "  med_time[ms]: " << suml[i][j] << std::endl;
            }
        }

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                outputm << "dens: " << dens[i] << "  num_of_verts: " << vertices[j] << "  med_time[ms]: " << summ[i][j] << std::endl;
            }
        }
        break;
    }
    }
    return 0;
}